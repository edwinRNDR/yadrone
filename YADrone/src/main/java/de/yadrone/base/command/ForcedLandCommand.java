package de.yadrone.base.command;

public class ForcedLandCommand extends RefCommand {
	public ForcedLandCommand() {
		super(false, false);
		// 9th bit set to 0
	}

    @Override
    public Priority getPriority() {
        return Priority.MAX_PRIORITY;
    }
}
